using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Remote;

namespace Content_Deletor
{
    class Video
    {

        RemoteWebDriver driver;

        String userName = "";
        String password = "";

        public void setup()
        {
            ChromeOptions disbalenotifs = new ChromeOptions();
            disbalenotifs.AddArguments("--disable-popup-notification, --disable-notifications");

            driver = new ChromeDriver(disbalenotifs);
        }

        public void content_search(String Query, String element_to_be_deleted)
        {

            driver.Navigate().GoToUrl("https://facebook.com");

            IWebElement user = driver.FindElement(By.Id("email"));
            user.SendKeys(userName);

            IWebElement pass = driver.FindElement(By.Id("pass"));
            pass.SendKeys(password);

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(100);
                driver.Keyboard.SendKeys(OpenQA.Selenium.Keys.Enter);

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(100);

            var URL = "https://www.facebook.com/groups/animemonk";
            var search = "/search/?query=";
            int y_axis = 500;
            do
            {

                driver.Navigate().GoToUrl(URL + search + Query);
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(100);

                if (isElementPresent(By.LinkText(element_to_be_deleted)))
                {
                    driver.FindElement(By.LinkText(element_to_be_deleted)).Click();
                    DeleteandRemoveUser();
                }
                else
                {
                    IJavaScriptExecutor jse = (IJavaScriptExecutor)driver;
                    jse.ExecuteScript("window.scrollBy(0," + y_axis + ")", "");
                    y_axis = y_axis + 500;

                }
            } while (!isElementPresent(By.XPath("//*[@id=\"browse_end_of_results_footer\"]/div/div/div")));


        }

        public bool isElementPresent(By by)
        {
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }

        }

        public void DeleteandRemoveUser()
        {
            driver.FindElement(By.ClassName("_4xev")).Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(100);

            driver.FindElement(By.XPath("//*[@id=\"post_menu\"]/div/ul/li[9]/a/span/span/div")).Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(100);

            var id = driver.FindElement(By.ClassName("_s")).GetAttribute("id");
            Thread.Sleep(500);

            driver.FindElement(By.CssSelector("#" + id + "> div._5lnf.uiOverlayFooter._5a8u > table > tbody > tr > td._51m-.uiOverlayFooterButtons._51mw > button")).Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(100);
        }
    }
}
